################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/DTH22.c" \
"../Sources/IIC.c" \
"../Sources/MCU.c" \
"../Sources/UCODErom.c" \
"../Sources/main.c" \
"../Sources/time.c" \

C_SRCS += \
../Sources/DTH22.c \
../Sources/IIC.c \
../Sources/MCU.c \
../Sources/UCODErom.c \
../Sources/main.c \
../Sources/time.c \

OBJS += \
./Sources/DTH22_c.obj \
./Sources/IIC_c.obj \
./Sources/MCU_c.obj \
./Sources/UCODErom_c.obj \
./Sources/main_c.obj \
./Sources/time_c.obj \

OBJS_QUOTED += \
"./Sources/DTH22_c.obj" \
"./Sources/IIC_c.obj" \
"./Sources/MCU_c.obj" \
"./Sources/UCODErom_c.obj" \
"./Sources/main_c.obj" \
"./Sources/time_c.obj" \

C_DEPS += \
./Sources/DTH22_c.d \
./Sources/IIC_c.d \
./Sources/MCU_c.d \
./Sources/UCODErom_c.d \
./Sources/main_c.d \
./Sources/time_c.d \

C_DEPS_QUOTED += \
"./Sources/DTH22_c.d" \
"./Sources/IIC_c.d" \
"./Sources/MCU_c.d" \
"./Sources/UCODErom_c.d" \
"./Sources/main_c.d" \
"./Sources/time_c.d" \

OBJS_OS_FORMAT += \
./Sources/DTH22_c.obj \
./Sources/IIC_c.obj \
./Sources/MCU_c.obj \
./Sources/UCODErom_c.obj \
./Sources/main_c.obj \
./Sources/time_c.obj \


# Each subdirectory must supply rules for building sources it contributes
Sources/DTH22_c.obj: ../Sources/DTH22.c
	@echo 'Building file: $<'
	@echo 'Executing target #1 $<'
	@echo 'Invoking: HCS08 Compiler'
	"$(HC08ToolsEnv)/chc08" -ArgFile"Sources/DTH22.args" -ObjN="Sources/DTH22_c.obj" "$<" -Lm="$(@:%.obj=%.d)" -LmCfg=xilmou
	@echo 'Finished building: $<'
	@echo ' '

Sources/%.d: ../Sources/%.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/IIC_c.obj: ../Sources/IIC.c
	@echo 'Building file: $<'
	@echo 'Executing target #2 $<'
	@echo 'Invoking: HCS08 Compiler'
	"$(HC08ToolsEnv)/chc08" -ArgFile"Sources/IIC.args" -ObjN="Sources/IIC_c.obj" "$<" -Lm="$(@:%.obj=%.d)" -LmCfg=xilmou
	@echo 'Finished building: $<'
	@echo ' '

Sources/MCU_c.obj: ../Sources/MCU.c
	@echo 'Building file: $<'
	@echo 'Executing target #3 $<'
	@echo 'Invoking: HCS08 Compiler'
	"$(HC08ToolsEnv)/chc08" -ArgFile"Sources/MCU.args" -ObjN="Sources/MCU_c.obj" "$<" -Lm="$(@:%.obj=%.d)" -LmCfg=xilmou
	@echo 'Finished building: $<'
	@echo ' '

Sources/UCODErom_c.obj: ../Sources/UCODErom.c
	@echo 'Building file: $<'
	@echo 'Executing target #4 $<'
	@echo 'Invoking: HCS08 Compiler'
	"$(HC08ToolsEnv)/chc08" -ArgFile"Sources/UCODErom.args" -ObjN="Sources/UCODErom_c.obj" "$<" -Lm="$(@:%.obj=%.d)" -LmCfg=xilmou
	@echo 'Finished building: $<'
	@echo ' '

Sources/main_c.obj: ../Sources/main.c
	@echo 'Building file: $<'
	@echo 'Executing target #5 $<'
	@echo 'Invoking: HCS08 Compiler'
	"$(HC08ToolsEnv)/chc08" -ArgFile"Sources/main.args" -ObjN="Sources/main_c.obj" "$<" -Lm="$(@:%.obj=%.d)" -LmCfg=xilmou
	@echo 'Finished building: $<'
	@echo ' '

Sources/time_c.obj: ../Sources/time.c
	@echo 'Building file: $<'
	@echo 'Executing target #6 $<'
	@echo 'Invoking: HCS08 Compiler'
	"$(HC08ToolsEnv)/chc08" -ArgFile"Sources/time.args" -ObjN="Sources/time_c.obj" "$<" -Lm="$(@:%.obj=%.d)" -LmCfg=xilmou
	@echo 'Finished building: $<'
	@echo ' '


