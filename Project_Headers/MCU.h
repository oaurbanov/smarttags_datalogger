/*
 * MCU.h
 *
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */

#ifndef MCU_H_
#define MCU_H_

typedef unsigned char UINT8;
typedef unsigned int  UINT16;

//ya que desactiva BKGD, por seguridad no comparto esta funcion fuera del scope de MCU.c
//void MCU_SetWaitMode(void);
void MCU_SetActiveMode(void);
void MCU_WaitModeForSecs(UINT16 secs);

void MCU_Initialize(void);
void MCU_GreenLedPower(UINT8 in);
void MCU_RedLedPower(UINT8 in);
void MCU_GreenLedBeating(UINT8 cant);
void MCU_RedLedBeating(UINT8 cant);




#endif /* MCU_H_ */
