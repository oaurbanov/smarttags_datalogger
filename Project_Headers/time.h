/*
 * time.h
 *
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */

#ifndef TIME_H_
#define TIME_H_

typedef unsigned char UINT8;
typedef unsigned int  UINT16;


void time_Wait(UINT16 time);

void time_sSleep(UINT16 secs);

void time_mSleep(UINT16 secs);

#endif /* TIME_H_ */
