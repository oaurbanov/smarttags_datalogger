/*
 * UCODErom.h
 *
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */

#ifndef UCODEROM_H_
#define UCODEROM_H_

typedef unsigned char UINT8;
typedef unsigned int  UINT16;


UINT8 UROM_readByte( UINT16 address );

UINT16 UROM_readWord( UINT16 address );

//void UROM_writeByte(UINT16 address, UINT8 dato);//no funciona para UCODE porque guarda WORD accediendo a direcciones par

void UROM_WriteWord(UINT16 address, UINT16 dato);

void UROM_EraseAll(void);

UINT16 UROM_getAddress_Temp(UINT16 temperature);//obtiene la direcci�n de memoria que hay que modificar en funci�n de la temperatura

void UROM_SaveTemp(UINT16 temperature); 

#endif /* UCODEROM_H_ */
