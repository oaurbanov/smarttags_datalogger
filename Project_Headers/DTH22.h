/*
 * DTH22.h
 *
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */

#ifndef DTH22_H_
#define DTH22_H_

typedef unsigned char UINT8;
typedef unsigned int  UINT16;


void DTH22_Power(UINT8 in);
void DTH22_StartData(void);
void DTH22_EnableDebugSignal(void);
void DTH22_GetData(void);
UINT16 DTH22_GetTemperature(void);


#endif /* DTH22_H_ */
