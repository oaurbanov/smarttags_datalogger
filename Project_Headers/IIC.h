/*
 * IIC.h
 *
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */

#ifndef IIC_H_
#define IIC_H_

typedef unsigned char UINT8;
typedef unsigned int  UINT16;


void IIC_Config(void);

void IIC_Start(void);

void IIC_Stop(void);

void IIC_Ack(void);

void IIC_PutByte(UINT8 data);

UINT8 IIC_GetByte(void); 




#endif /* IIC_H_ */
