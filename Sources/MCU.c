/*
 * MCU.c
 * gesti�n de leds y modos de energ�a del MCU
 * 
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */

#include "MCU.h"
#include "derivative.h" /* include peripheral declarations */
#include "time.h"

typedef unsigned char UINT8;
typedef unsigned int  UINT16;

UINT8 lowPowerEnabled = 0;

extern const UINT8 ON;
extern const UINT8 OFF;

static const UINT16 BEATING_TIME = 400; //ms

//ya que desactiva BKGD, por seguridad no comparto esta funcion fuera del scope de MCU.c
void MCU_SetWaitMode(){
	/*	FLL Bypassed Internal Low Power (FBILP)
	  	The FLL bypassed internal low power (FBILP) mode is entered when all the following conditions occur:
		� CLKS bits are written to 01
		� IREFS bit is written to 1.
		� BDM mode is not active and LP bit is written to 1
		In FLL bypassed internal low power mode, the ICSOUT clock is derived from the internal reference clock
		and the FLL is disabled. The ICSLCLK will be not be available for BDC communications, and the internal
		reference clock is enabled.*/
		
		ICSC1_CLKS =1; //Internal reference clock is selected.
		ICSC1_IREFS = 1; //Internal reference clock selected
		ICSC2_LP = 1; //FLL is disabled in bypass modes unless BDM is active
		//ICSC2_BDIV   = 3;//divide internal clock by 8 (8MHz bus)
		//ICSTRM = 0x00; //modificar los TRM no funcion� para ajustar frec reloj al maximo
		//ICSSC_FTRIM = 0; // The FTRIM bit controls the smallest adjustment of the internal reference clock frequency.
		
		lowPowerEnabled = 1;
}

void MCU_SetActiveMode(){
	/*	FLL Engaged Internal (FEI)
		FLL engaged internal (FEI) is the default mode of operation out of any reset and is entered when all the
		following conditions occur:
		� CLKS bits are written to 00
		� IREFS bit is written to 1
		� RDIV bits are written to divide reference clock to be within the range of 31.25 kHz to 39.0625 kHz.
		In FLL engaged internal mode, the ICSOUT clock is derived from the FLL clock, which is controlled by
		the internal reference clock. The FLL loop will lock the frequency to 512 times the filter frequency, as
		selected by the RDIV bits. The ICSLCLK is available for BDC communications, and the internal reference
		clock is enabled.*/

		ICSC1_CLKS =0;	//Output of FLL is selected.
		ICSC1_IREFS = 1; //Internal reference clock selected
		ICSC1_RDIV = 0; //Divides reference clock by 1 (reset default)
	    ICSC2_BDIV   = 0;//divide internal clock by 1 (8MHz bus)
		//ICSC2_LP = 0;
	    
	    lowPowerEnabled = 0;
}

void MCU_WaitModeForSecs(UINT16 secs){ 
	UINT8 j;
	
	//MCU_GreenLedBeating(2);
	MCU_SetWaitMode();

	time_sSleep(secs);
	//for (j=0; j<secs; j++){
	//	time_sSleep(1);
	//}
	
	MCU_SetActiveMode();
	
}


void MCU_Initialize(){	//configuro micro
	SOPT1=0x02;	//watchdog disabled, bkgd enabled, IRQ external interrupt enabled
    ICSC2_BDIV   = 0;//divide internal clock by 1 (8MHz bus)
	PTBDD=0xFE;		//habilita las 7 salidas de PTB 7-1   0:in
	PTBD=0x01;		//inicializa PTBD=0;
	PTADD=0xFD;		//habilita entrada PTA1
	PTAD=0x02;		//inicializa PTAD=0;    
}



void MCU_GreenLedPower(UINT8 in){
	PTBDD_PTBDD4 = 1; //salida=1, entrada=0
	PTBD_PTBD4 = in; 			
}

void MCU_RedLedPower(UINT8 in){
	PTBDD_PTBDD5 = 1; //salida=1, entrada=0
	PTBD_PTBD5 = in;	
}

void MCU_GreenLedBeating(UINT8 cant){
	UINT8 i;
	for(i=0;i<cant;i++){
		MCU_GreenLedPower(OFF);
		time_mSleep(BEATING_TIME);	
		MCU_GreenLedPower(ON);
		time_mSleep(BEATING_TIME);			
	}
	MCU_GreenLedPower(OFF);
}

void MCU_RedLedBeating(UINT8 cant){
	UINT8 i;
	for(i=0;i<cant;i++){
		MCU_RedLedPower(OFF);
		time_mSleep(BEATING_TIME);	
		MCU_RedLedPower(ON);
		time_mSleep(BEATING_TIME);			
	}
	MCU_RedLedPower(OFF);
}




