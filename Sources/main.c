//%%%%%%%%%%%%%%%%%%%% SmartTag %%%%%%%%%%%%%%%%%%%%%%%%%%%

/*********************************************************************************
* Originator: Oscar Urbano 										 *
* Date: 30 08 2017 														 *
* Function: Por medio de la comunicacion IIC este programa comunica una memoria	 *
* PROM de 512K y el reoj DS1307 con el microcontrolador MC9S08SE8.				 *
**********************************************************************************/


#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "IIC.h"
#include "time.h"
#include "DTH22.h"
#include "UCODErom.h"
#include "MCU.h"

typedef unsigned char UINT8;
typedef unsigned int  UINT16;

//static const UINT8 ON = 1; //el static me asegura que esta variable se define solo para esta translation unit (hoja)
const UINT8 ON = 1; //sin estatic, luego las llamo con extern en los otros .c donde las quiero utilizar
const UINT8 OFF = 0;

const UINT8 sensorType = 0x00; //Temperatura
const UINT16 TIME_TAKING_MEASURES = 2; //secs, tiempo que se demora leyendo sensor y acualizando la UCODEROM


UINT16 ttLog = 0; //en segundos
UINT8 auxTtLog = 0;//guarda los dos bits para el ttLog
UINT8 configByte = 0;

extern UINT8 lowPowerEnabled;

void SmartTag_UpdateTtlog(void){
	//extraigo ttLog y lo clasifico
	auxTtLog = configByte & 0x03;
	//estos 4 casos para timeToLogg
	if (auxTtLog == 0x03) ttLog = 3600; //60 min
	else if (auxTtLog == 0x02) ttLog = 1800; //30 min
	else if (auxTtLog == 0x01) ttLog = 600; //10 min
	else if (auxTtLog == 0x00) ttLog = 300; //5 min
}

void SmartTag_Init(void){
	UINT8 newConfigByte;
	UINT16 newConfigWord;
	
	configByte = UROM_readByte( 0x6000 );

	if ( (configByte & 0x80 ) == 0x80 ){
		SmartTag_UpdateTtlog(); //extraigo ttLog y lo clasifico
		return; //ya he le�do byte de configuracion. Regreso
	}
	else {
		SmartTag_UpdateTtlog();//extraigo ttLog y lo clasifico			
		newConfigByte = 0x80 | sensorType << 2 | auxTtLog;//armo configByte = leido + sensorType + ttLog
		newConfigWord = newConfigByte;
		newConfigWord = newConfigWord<<8;
		UROM_EraseAll();
		UROM_WriteWord(0x6000, newConfigWord);			
	}		
		
}

//0x60B0//8.0  -   0x617E//28.0 = 100 words
UINT8 SmartTag_ReadRom_Debug(void){
	UINT16 dataROM[40];//sacarlo al global scope si no aparece durante el debugging---------------meter dentro de SmartTag_ReadRom_Debug(void) para version produccion
	UINT16 q=0;
	UINT8 j=0;
	UINT16 start  = 0x612E; //20.0
	UINT16 end  = 0x617E;	//28.0
	
	j=0;
	for(q=start ; q<end; q=q+2){
		dataROM[j] = UROM_readWord(q);
		j++;
	}
	
	return 0;
}


UINT16 temperature=0;
void main(void){

MCU_Initialize();
MCU_SetActiveMode();
MCU_RedLedBeating(3); // marca inicio de c�digo

MCU_GreenLedPower(OFF);
MCU_RedLedPower(OFF);

//SmartTag_ReadRom_Debug();//para verificar escritura.----------comentar para version produccion
//UROM_EraseAll();//descomentar solo cuando se quiera borrar memoria //ejecuto una vez solo antes de mandar a produccion ----------comentar para version produccion

UROM_WriteWord(0x6000, 0x8000); //inicializaci�n de tag artificial, ----------0x6000, 0x8000 para 1er version produccion
SmartTag_Init();

time_sSleep(10); //tiempo para reprogramar MCU en caso de que algo falle con waitMode que desactiva BKGD

for(;;){
	
	MCU_WaitModeForSecs(ttLog - TIME_TAKING_MEASURES);//OJO: Cuidado que desactiva el bkgd y luego no se puede programar MCU


	//obtengo temperatura
	MCU_GreenLedPower(ON);
	temperature = DTH22_GetTemperature();
	MCU_GreenLedPower(OFF);

	//guardo temperatura en UCODE_ROM
	MCU_RedLedPower(ON);
	UROM_SaveTemp(temperature);
	MCU_RedLedPower(OFF);

	
	
	//verificaci�n de tiempos
	//time_sSleep(10);
	//MCU_RedLedPower(ON);
	//MCU_WaitModeForSecs(60);//verificando waiMode
	//time_sSleep(60); //verificando activeMode
	//MCU_RedLedPower(OFF);
}	
}


