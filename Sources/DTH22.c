/*
 * DTH22.c
 *
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */

#include "DTH22.h"
#include "derivative.h" /* include peripheral declarations */
#include "time.h"

typedef unsigned char UINT8;
typedef unsigned int  UINT16;

extern const UINT8 ON;
extern const UINT8 OFF;

static const DTH22_debugging = 1;

// guarda la trama de datos entregada por el DTH22
UINT8 DTH22_Data[5] = {0xff,0xff,0xff,0xff,0xff}; //{HighHumidity, LowHumidity, HighTemperature, LowTemperature, ParityByte}

//EXAMPLE DTH22_DATA
// Data received:
//   0000 0010 		 1001 0010	      0000 0001 		0000 1101 	      1010 0010
// High humidity   Low humidity    High temperature    Low temperature    Parity byte
// Calculate:
// 0000 0010+1001 0010 +0000 0001+0000 1101= 1010 0010(Parity bit) => Data is correct
// Humidity: 0000 0010 1001 0010 = 0x0292 = 2�256 + 9�16 + 2 = 658 (decimal)  => Humidity = 65.8%RH
// Temp.:0000 0001 0000 1101 = 10DH(Hexadecimal) = 1�256 + 0�16 + 13 = 269 (decimal) => Temp.= 26.9?




void DTH22_Power(UINT8 in){
	PTBDD_PTBDD2 = 1; //salida=1, entrada=0 //pin de energ�a del sensor
	PTBDD_PTBDD3 = 1; //salida=1, entrada=0 //este es el pin por donde entrar� la trama de datos del sensor

	if(in == OFF){
		PTBD_PTBD2 = 0; // sensorOFF		
		PTBD_PTBD3 = 0;
	}else if (in == ON){
		PTBD_PTBD2 = 1; // sensorON
		PTBD_PTBD3 = 1;//Dejo en alto para luego iniciarlo con el bajo // No startDataSensor, no es necesario
		time_mSleep(2000);//este wait le da un tiempo al sensor para que  se encienda y pueda escuchar la se�al de inicio		
	}
}

void DTH22_StartData(){
	// startDataSensor
	PTBDD_PTBDD3 = 1; //salida=1, entrada=0
	PTBD_PTBD3 = 0; // startDataSensor con un bajo 
	time_mSleep(20);
	PTBDD_PTBDD3 = 0; //salida=1, entrada=0
	if (DTH22_debugging) DTH22_EnableDebugSignal();
}

void DTH22_EnableDebugSignal(void){
	//aux // para mostrar en el oscilador la frecuencia de las operaciones
	PTBDD_PTBDD1 = 1; //salida=1, entrada=0
	PTBD_PTBD1 = 0;//aux //lo ten�a en el d2 pero lo cambi� un pin mas arriba porque era el mismo de encender el sensor		
}




void DTH22_GetData(){
	UINT16 q=0;
	UINT8 count=0x00;
	UINT8 index =0x00;
	UINT16 Data1=0; //bits menos significativos
	UINT16 Data2=0;
	UINT16 Data3=0; //bits mas significativos
	UINT8 suma=0;
	
	DTH22_Power(ON);

	do{
		
		DTH22_StartData();	
		while (!PTBD_PTBD3);
		//La informacion del sensor es enviada en 40 bits.
		// el sensor empieza enviando dos bits en altos que es la se�al de inicio
		// luego envia los bits del mas significativo hasta el menos significativo
		// al final desplazo 1 byte al ultimo registro para tener toda la informacion
		// ordenada como la recibo.
		count=0; index =0; Data1=0; Data2=0; Data3=0;
		for( q=0; q<1000; q++){
			if (DTH22_debugging) PTBD_PTBD1 = ~PTBD_PTBD1; //AUX
			
			if (PTBD_PTBD3){
				count++;
			}
			else{
				if(count>7){//es un uno
					if (DTH22_debugging) PTBD_PTBD1 = 1; //AUX
					index++;
					count=0;
					if (index>2 && index<=18){ //de los dos altos del inicio de trama se detecta solo el segundo, porque el primero es muy corto
						Data3 = Data3 << 1;
						Data3 = Data3 | 0x01;
					}else if (index<=34){
						Data2 = Data2 << 1;
						Data2 = Data2 | 0x01;					
					}else if (index<=42){
						Data1 = Data1 << 1;
						Data1 = Data1 | 0x01;					
					}
				}else if (count>0){ //es un cero
					if (DTH22_debugging) PTBD_PTBD1 = 0; //AUX
					index++;
					count=0;
					if (index>2 && index<=18){
						Data3 = Data3 << 1;
					}else if (index<=34){
						Data2 = Data2 << 1;
					}else if (index<=42){
						Data1 = Data1 << 1;
					}
				}
			}
	
		}
	
		DTH22_Data[4] = Data3 >> 8; //primeros bits que me entrega el sensor //High humidity
		DTH22_Data[3] = Data3;												 //Low humidity
		DTH22_Data[2] = Data2 >> 8;											 //High Temperature
		DTH22_Data[1] = Data2;												//Low Temperature
		DTH22_Data[0] = Data1;		// ultimo byte, el de crc				//Parity byte
		
		suma=0x00;
		for(q=1;q<=4;q++){
			suma = suma + DTH22_Data[q];
		}
	}while( DTH22_Data[0] != suma  ); //check error implementation	
	
	
	DTH22_Power(OFF);
}

UINT16 DTH22_GetTemperature(){
	UINT16 temperature = 0x0000;
	UINT16 temperatureAux = 0x0000;
	
	DTH22_GetData();//extraigo datos del sensor

	//0000 0000 0110 0101 = 0065H(Hexadecimal)= 6�16 +5 = 101
	temperatureAux = DTH22_Data[2]; //high temp
	temperature = DTH22_Data[1]; //low temp
	
	temperature = (temperatureAux << 8) | temperature;
	
	return temperature;
}

