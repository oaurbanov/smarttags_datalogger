/*
 * time.c
 *
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */
#include "time.h"
#include "MCU.h"

typedef unsigned char UINT8;
typedef unsigned int  UINT16;

extern UINT8 lowPowerEnabled;

// Esta funcion no tiene soporte para cuando lowPowerEnabled porque fue hecha antes de implementar la funcionalidad de waitMode()
//De todas formas no se utiliza durante waitMode, solo durante activeMode
void time_Wait(UINT16 time){
	UINT16 i;
	for(i=0;i<time;i++);
}

//Esta funci�n se utiliza solo en activeMode
void time_mSleep(UINT16 secs){
	UINT16 i;
	for (i=0;i<secs;i++){
		time_Wait(356);//20//18   //200 en ActiveMode
	}
}

//Esta es la unica funci�n que se utiliza durante waitMode
//Se utiliza tambi�n durante activeMode
void time_sSleep(UINT16 secs){
	UINT16 i, j;
	for (j=0; j<secs; j++){
		if(lowPowerEnabled ==1){//durate waitMode
			for (i=0;i<720;i++){} //1430 valor en waitMode // 1 sec //deber�a ser 32000 porque el reloj interno corre a 32k, pero lo ajuste manualmente as�
		}
		else time_mSleep(1000);//durante ActiveMode
	}
}
