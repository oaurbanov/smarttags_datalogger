/*
 * IIC.c
 *
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */

#include "IIC.h"
#include "derivative.h" /* include peripheral declarations */
#include "time.h"

typedef unsigned char UINT8;
typedef unsigned int  UINT16;
extern const UINT8 ON; //extern le dice al compilador que fue declarada en otro achivo que la busque
extern const UINT8 OFF;
static const UINT16 TIME_TO_WAIT = 100; //el static me asegura que esta variable se define solo para esta translation unit (hoja)

void IIC_Config(void){
	IICF = 0x99; // Este instrucción selecciona la velocidad del Bus /0x99
	IICA =0x01; //Pone como dirección de esclavo del qg8 (cuandO se necesite) en 0x00
}

void IIC_Start(void){
	IICC_IICEN = 1; // Activa el modulo IIC
	IICC_TXAK = 1; // genera un bit de reconocimiento ACK despues de recibir
	IICC_MST = 1; //Comunicación como maestro
	IICS_SRW = 0; //Bit R/W nivel bajo, modo Escritura. 
	time_Wait(TIME_TO_WAIT);	//espera
}

void IIC_Stop(void){
	IICC_MST = 0; //genera Un Stop
	time_Wait(TIME_TO_WAIT);	//espera
}

void IIC_Ack(void){
	IICC_TXAK = 0; // acknowledge bit sent
	time_Wait(TIME_TO_WAIT); // 50 us/unidad  ;  1 seg = 20 000
}

void IIC_PutByte(UINT8 data){
	int count =0;
	//IICC_TXAK = 0; //ackonwledge Bit signal enviada despues de recibir byte
	IICC |= 0x30; // MST=1 modo maestro, TX=1 modo transmisión
	IICD = data;  // Dato el cual va ha trasmitir. 
	while (!IICS_IICIF){ //posee una interrupción pendiente 
		count ++;// timeout para que no se quede aqui trabado
		if (count==2000){ //2000 = 100 mSeg
			count=0;
			break;
		}
	}
	IICS_IICIF=1;			//borra el flag de interrupción 
	while(IICS_RXAK){	//Espera la llegada del ACK 
		count ++;// timeout para que no se quede aqui trabado
		if (count==2000){ 
			count=0;
			break;
		}
	}
//	while(IICS_BUSY); // mietras el bus esta ocupado esperar a que libere el bus
}

UINT8 IIC_GetByte(void)
{
	UINT8 Data;

	IICC_RSTA = 1;		// repeat start condition
	time_Wait(TIME_TO_WAIT);	//espera

	IICD = 0xA3;//0xA3	//reg IICD es el data I/O register
	while (!IICS_IICIF);	// one byte transfer completes
	IICS_IICIF=1; 
	while (IICS_RXAK);		//==1 == No acknowledge received
	
	IICC_TX = 0; // set up to receive;
	Data = IICD; // dummy read;  falsa lectura
	while (!IICS_IICIF); // wait one byte transfer completes
	
	IICS_IICIF=1; // clear the interrupt event flag;
	Data = IICD; // read right data;
	while (!IICS_IICIF); // wait one byte transfer completes

	return Data;
}

