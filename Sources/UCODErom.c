/*
 * UCODErom.c
 *
 *  Created on: Nov 4, 2017
 *      Author: oskra
 */

#include "UCODErom.h"
#include "IIC.h"
#include "time.h"
#include "MCU.h"

typedef unsigned char UINT8;
typedef unsigned int  UINT16;

extern const UINT8 ON;
extern const UINT8 OFF;

UINT8 UROM_readByte( UINT16 address ){
	UINT8 dato = 0x00;
	
	UINT8 address2 = address>>8 & 0xFF; //mas significativo
	UINT8 address1 = address & 0xFF; //menos significativo
	IIC_Start();
	IIC_PutByte(0xA2);
	IIC_PutByte(address2); //mas significativo
	IIC_PutByte(address1); //menos significativo

	dato= IIC_GetByte();

	IIC_Ack();
	IIC_Stop();
	
	return dato;
	
}

UINT16 UROM_readWord( UINT16 address ){ //falta comprobar esta funci�n
	UINT8 dato2 ; //mas significativo
	UINT8 dato1 ; //menos significativo
	UINT16 dato ;
	UINT16 datoAux = 0x0000;
	
	dato2 = UROM_readByte( address );
	dato1 = UROM_readByte( address+1 );//esto funciona porque al no poder acceder al direcci�n de memoria imparme accede a la que le sigue a la anterior que es la misma que necesito
	
	dato = dato2;
	dato = dato<<8;
	datoAux = datoAux | dato1;
	dato = dato | datoAux;

	return dato;
	
}

void UROM_WriteWord(UINT16 address, UINT16 dato){
	UINT8 dato2 = dato>>8 & 0xFF; //mas significativo
	UINT8 dato1 = dato & 0xFF; //menos significativo
	UINT8 address2 = address>>8 & 0xFF; //mas significativo
	UINT8 address1 = address & 0xFF; //menos significativo
	IIC_Start();
	IIC_PutByte(0xA2);
	IIC_PutByte(address2); //mas significativo
	IIC_PutByte(address1); //menos significativo
	IIC_PutByte(dato2); //mas significativo
	IIC_PutByte(dato1);//menos significativo
	IIC_Stop();
}

//no funciona para UCODE porque guarda WORD accediendo a direcciones par
/*void UROMWriteByte(UINT16 address, UINT8 dato){
	UINT8 address2 = address>>8 & 0xFF; //mas significativo
	UINT8 address1 = address & 0xFF; //menos significativo
	IIC_Start();
	IIC_PutByte(0xA2);
	IIC_PutByte(address2); //mas significativo
	IIC_PutByte(address1); //menos significativo
	IIC_PutByte(dato);
	IIC_Stop();
}*/

void UROM_EraseAll(){
	UINT16 i;

	MCU_GreenLedPower(ON);/////aviso de borrado de memoria
	MCU_RedLedPower(ON);/////aviso de borrado de memoria

	IIC_Config(); // Configuro IIC
	for(i=0x6000;i<=0x619E; i++){ //aumenta de a uno porque de a 2 me estaba borrando saltando una palabra
		UROM_WriteWord(i, 0x0000);
	}	

	MCU_GreenLedPower(OFF);/////aviso de borrado de memoria
	MCU_RedLedPower(OFF);/////aviso de borrado de memoria
}


//encuentra RegistroEquivalente, lee valor de Registro,le suma 1 y lo vuelve a guardar
void UROM_SaveTemp(UINT16 temperature){
	UINT16 valueReg=0;
	UINT16 equivalentAddress=0;

	equivalentAddress = UROM_getAddress_Temp(temperature);
	valueReg = UROM_readWord( equivalentAddress );
	valueReg++;
	UROM_WriteWord(equivalentAddress, valueReg);	
}



//en funci�n al mapa de memoria en este caso para temperatura, encuentra el registro equivalente
UINT16 UROM_getAddress_Temp(UINT16 temperature){
	UINT16 equivalentAddress;
	UINT16 address;
	UINT8 negTemp;
	
	temperature = temperature>>1;
	temperature = temperature<<1;//con esto vuelvo el ultimo bit cero, con lo que hago todos los valores pares
	
	if ( (temperature & 0x8000)==0x8000 ){//el bit mas significativo est� en 1, es temperatura negativa
		temperature = temperature<<1;
		temperature = temperature>>1;//con esto le quito el signo negativo a temperatura
		negTemp = 1;
	}else{//el bit mas significativo est� en 0, es temperatura positiva
		negTemp = 0;
	}
	
	if(negTemp==1){ //para temperaturas negativas
		
		if(temperature>100) address = 0x6002 ; //0x61 148 //para temperaturas < -10
		else  address = 0x6004 + (100 - temperature);		
		
	}else if(negTemp==0){ //para temperaturas positivas
		
		if (temperature>300) address = 0x6196 ; //0x61 150 //para temperaturas > 30
		else address = 0x6068 + temperature;

	}
	
	equivalentAddress = address;
	return equivalentAddress;
	
}


//as� escrib�a cuando era datalogger
/*void escrituraProgresiva(){
	//ver cuales borro

	UINT8 indexMem1=0x60, indexMem2=0;		//numero de pulsasiones de pulsador 1
	UINT8 datos[4]={6,5,4,3};
	UINT16 dataPROM[4]=0;		//lee los datos de la PROM

	int q=0; int k=0;
	UINT8 count=0x00, j=0x00; 
	UINT8 index =0x00, aux = 0x00; 

	UINT8 sensorData[6] = {0xff,0xff,0xff,0xff,0xff,0xff};
	UINT8 suma=0x00;
	UINT8 indexMeasure = 0x00;

	int Data1=0; //bits menos significativos
	int Data2=0;
	int Data3=0; //bits mas significativos

	
	// Esta secci�n de c�digo escribe la memoria del UCODE progresivamente
	// hasta la direcci�n 0x619E y luego vuelve a 0x6000
		
		IIC_Config(); // Configuro IIC
		for( q=0; q<6; q=q+2){			//escribe datos en la EEPROM 
			if( (indexMem1==0x60)&&(indexMem2==0x00) ){
				sensorData[5] = 0xF5; //primeros bits que me entrega el sensor
				sensorData[4] = 0xF4;
				sensorData[3] = 0xF3;
				sensorData[2] = 0xF2;
				sensorData[1] = 0xF1;  //ultimos bits que me entrega el sensor
				sensorData[0] = 0xF0;		// byte de relleno FF
			}
			writeRomUCODE(indexMem1, indexMem2, sensorData[q], sensorData[q+1]);
			time_mSleep(20);

			if (indexMem1==0x60 && indexMem2==254){
				indexMem1=0x61;
			}
			if(indexMem1==0x61 && indexMem2==0x9E){
				indexMem1=0x60;
				indexMem2=0x00;
				break;//rompo for porque al principio de la memoria debo escribir una trama entera
			}else {
				indexMem2=indexMem2+2;			
			}
		}

}*/
